block-outside-container
=======================

This is a companion add-on for [Firefox Multi-Account
Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)
which goes a step further and allows you to block all content for specific sites from
loading except in their designated containers. That way, blocked sites that are embedded
in sites loaded in other containers (e.g. Facebook "Like" buttons, Twitter buttons, etc.)
cannot see what you are doing outside their containers.

Another issue this solves is that althought Multi-Account Containers allows you to specify
that a site should always open in a specific container, it does not work if you want to
use that site in multiple containers, for example, to manage multiple accounts. With this
extension, when a site is blocked, you can choose from multiple containers to load the
site in.

Installation
------------

The extension is available on the Firefox Add-ons site [here](https://addons.mozilla.org/en-US/firefox/addon/block-sites-outside-container/).

Building
--------

This extension requires [make_webext.py](https://gitlab.com/ktpanda/make-webext). To build, just run:

    ./make_webext.py block_outside_container.user.js
