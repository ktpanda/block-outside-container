// ==UserScript==
// @name        Block sites outside container
// @namespace   http://ktpanda.org/monkey/
// @description Set up domains so that they aren't allowed to load content except in specific containers
// @version     1.5.5
// @icon        block_outside_container.png
// @manifest    author = "Katie Stafford"
// @manifest    developer = {}
// @manifest    developer.name = "Katie Stafford"
// @manifest    developer.url = "https://ktpanda.org/"
// @manifest    homepage_url = "https://gitlab.com/ktpanda/block-outside-container"
// @manifest    options_ui = {}
// @manifest    options_ui.page = "options.html"
// @manifest    options_ui.browser_style = true
// @manifest    options_ui.open_in_tab = false
// @manifest    applications.gecko.id = "block_outside_container@ktpanda.org"
// @permission  tabs
// @permission  webRequest
// @permission  webRequestBlocking
// @permission  cookies
// @permission  contextualIdentities
// @permission  storage
// @permission  <all_urls>
// ==/UserScript==

// ==Resource==
// @filename LICENSE
// ==/Resource==

//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.

// ==UserScript==
// @filename   background.js
// @run-at      background
// ==/UserScript==

/* JSLint declarations */
/* global browser */

/* ' */

'Use strict';

const message_url = browser.runtime.getURL('block_message.html');

var domain_container_rules = [];
var registered_listeners = new Map();

var all_domains = new Map();

function redirect_block_message(url, ruleid) {
    return { redirectUrl: message_url + '#' + [url, ruleid].map(encodeURIComponent).join(':') };
}

function save_rules() {
    return browser.storage.local.set({ domain_container_rules });
}

function get_rule_by_id(id) {
    return domain_container_rules.find(rule => rule.id === id);
}

async function load_rules() {
    ({ domain_container_rules } = await browser.storage.local.get({ domain_container_rules }));
    update_rules(true);
}

function get_rule_by_domain(domain) {
    let id = all_domains.get(domain);
    if (id !== undefined)
        return id;

    let components = domain.split('.');
    for (let i = 1; i < components.length; i++) {
        let subd = components.slice(i).join('.');
        let id = all_domains.get(subd);
        if (id !== undefined)
            return id;
    }

    return null;
}

function update_rules(update_all) {
    for (let rule of domain_container_rules) {
        if (update_all || rule.remove || rule.update || !rule.enabled) {
            let listener = registered_listeners.get(rule.id);
            if (listener) {
                console.log(`remove listener for rule ${rule.id}`);
                browser.webRequest.onBeforeRequest.removeListener(listener.listener, listener.args, ["blocking"] );
                registered_listeners.delete(rule.id);
            }
        }
    }

    domain_container_rules = domain_container_rules.filter(r => !r.remove && typeof(r.id) === 'number');
    all_domains = new Map();

    for (let rule of domain_container_rules) {

        if (!(rule.update || update_all))
            continue;

        rule.update = false;

        if (!rule.enabled)
            continue;

        let domain_list = rule.domains.split('\n').map(d => d.trim()).filter(d => d.length);
        if (!domain_list.length) {
            console.log(`rule ${rule.id} has no domains`);
            continue;
        }

        for (let domain of domain_list) {
            all_domains.set(domain, rule.id);
        }

        let listener = async details => {
            try {
                var tab = await browser.tabs.get(details.tabId);
            } catch (e) {
                console.log(`blocked request ${details.url} in tab ${details.tabId}`);
                var ret = redirect_block_message(details.url, rule.id);
                return ret;
            }

            if (!rule.containers.includes(tab.cookieStoreId)) {
                console.log(`blocked request ${details.url} in tab ${details.tabId}`);
                return redirect_block_message(details.url, rule.id);
            }
            return null;
        };
        var args = { urls: domain_list.map(domain => '*://*.' + domain + '/*') };
        console.log([`add listener for rule ${rule.id}`, args]);
        registered_listeners.set(rule.id, { listener, args });

        browser.webRequest.onBeforeRequest.addListener(listener, args, ["blocking"] );
    }
}

const actions = {
    get_containers: async (message, sender) => {
        var rv = {};

        // The name of the container that block message is running in
        var containername = "Default";

        var containers = (await browser.contextualIdentities.query({})).map(c => ({ id: c.cookieStoreId, name: c.name, color: c.colorCode, icon: c.iconUrl }));
        containers = [{id: "firefox-default", name: "Default", color: "#000000", icon: ""}, ...containers];

        var caller_container = containers.find(c => c.id == sender.tab.cookieStoreId);
        if (caller_container)
            containername = caller_container.name;

        if (message.rule_id) {
            rv.rule = get_rule_by_id(message.rule_id);
            var allowed_containers = [];
            if (rv.rule) {

                // Current container is allowed? Just reopen the
                // original URL. The user might have gone to settings,
                // changed the rule, then went back and hit refresh.
                allowed_containers = rv.rule.containers;
                if (allowed_containers.includes(sender.tab.cookieStoreId)) {
                    console.log({url:message.url});
                    await actions.reopen_in_container({ url: message.url, container: sender.tab.cookieStoreId}, sender);
                }
            }

            containers = containers.filter(c => allowed_containers.includes(c.id));
        } else {
            rv.domain_container_rules = domain_container_rules;
        }
        rv.containers = containers;
        rv.containername = containername;
        return rv;
    },

    reopen_in_container: async (message, sender) => {
        await browser.tabs.create({
            url: message.url,
            active: true,
            cookieStoreId: message.container,
            index: sender.tab.index + 1,
            openerTabId: sender.tab.openerTabId,
            windowId: sender.tab.windowId
        });

        // Only close the source tab when it is running in the top frame
        if (sender.frameId == 0)
            await browser.tabs.remove(sender.tab.id);
    },

    disable_and_reopen: async (message, sender) => {
        await actions.update_rule_enabled({
            rule_id: message.rule_id,
            enabled: false
        });

        await actions.reopen_in_container({
            url: message.url,
            container: sender.tab.cookieStoreId
        }, sender);
    },

    enable_current_and_reopen: async (message, sender) => {
        var rule = get_rule_by_id(message.rule_id);
        if (!rule)
            return;

        if (rule.containers.includes(sender.tab.cookieStoreId))
            return;

        rule.containers.push(sender.tab.cookieStoreId);
        await save_rules();

        await actions.reopen_in_container({
            url: message.url,
            container: sender.tab.cookieStoreId
        }, sender);
    },

    update_rule_containers: async (message, sender) => {
        var rule = get_rule_by_id(message.rule_id);
        if (rule) {
            rule.containers = message.containers;
            await save_rules();
        }
    },
    update_rule_name: async (message, sender) => {
        var rule = get_rule_by_id(message.rule_id);
        if (rule) {
            rule.name = message.name;
            await save_rules();
        }
    },
    update_rule_domains: async (message, sender) => {
        var rule = get_rule_by_id(message.rule_id);
        if (rule) {
            rule.domains = message.domains;
            rule.update = true;
            update_rules();
            await save_rules();
        }
    },
    update_rule_enabled: async (message, sender) => {
        var rule = get_rule_by_id(message.rule_id);
        if (rule) {
            rule.enabled = message.enabled;
            rule.update = true;
            update_rules();
            await save_rules();
        }
    },
    delete_rule: async (message, sender) => {
        var rule = get_rule_by_id(message.rule_id);
        console.log(`attempt to delete rule ${message.rule_id}`);
        console.log(["rule is", rule]);
        if (rule) {
            rule.remove = true;
            update_rules();
            await save_rules();
        }
    },
    new_rule: async (message, sender) => {
        let last_id = domain_container_rules.reduce((a, b) => a > b.id ? a : b.id, 0);
        var rule = { id: last_id + 1, enabled: true, name: `New Rule ${last_id + 1}`, domains: '', containers: [] };
        domain_container_rules.push(rule);
        update_rules();
        await save_rules();
        return rule;
    },
    open_settings: async (message, sender) => {
        browser.runtime.openOptionsPage();
    }
};

const external_actions = {
    rule_exists: async (message, sender) => {
        let domain = message.domain;
        if (typeof domain !== 'string') {
            throw new Error(`message.domain must be a string, not ${typeof domain}`);
        }
        return {domain: domain, rule_exists: get_rule_by_domain(domain) !== null};
    }
};

browser.runtime.onMessage.addListener((message, sender) => {
    if (actions.hasOwnProperty(message.action)) {
        return actions[message.action](message, sender);
    }
    return null;
});

browser.runtime.onMessageExternal.addListener((message, sender) => {
    if (typeof message.action !== 'string') {
        throw new Error(`message.action must be a string, not ${typeof message.action}`);
    }
    if (external_actions.hasOwnProperty(message.action)) {
        return external_actions[message.action](message, sender);
    }
    throw new Error(`Unknown action: ${message.action}`);
});

load_rules().catch(console.error);

// ==Resource==
// @filename    view_common.js
// ==/Resource==

function create_elem(tag, attrs) {
    var elem = document.createElement(tag);
    if (attrs) {
        for (attr in attrs) {
            if (attrs.hasOwnProperty(attr) && attr !== 'text')
                elem.setAttribute(attr, attrs[attr]);
        }
        if (attrs.text) {
            elem.appendChild(document.createTextNode(attrs.text));
        }
    }
    return elem;
}

function create_container_box({ color, icon, name }) {
    let link = create_elem('button', { class: 'container-link', style: `color: ${color}` });
    let inner = create_elem('div', { class: 'container-inner' } );
    link.appendChild(inner);
    if (icon) {
        var img = create_elem('span', {style: `background-image: url(${icon}); fill: ${color}`});
        inner.appendChild(img);
    } else {
        var img = create_elem('span', {class: 'noicon'});
        inner.appendChild(img);
    }
    inner.insertAdjacentText('beforeend', name);
    return link;
}

// ==Resource==
// @filename    block_message.js
// ==/Resource==

/* global URL */

async function init() {
    var [desturl, rule_id] = window.location.hash.substr(1).split(':');
    rule_id = parseInt(rule_id);

    desturl = decodeURIComponent(desturl);

    var { containers, rule, containername } = await browser.runtime.sendMessage({ action: 'get_containers', rule_id, url: desturl });

    containername = decodeURIComponent(containername);
    var host;
    try {
        host = new URL(desturl).host;
    } catch (e) {
        host = desturl;
    }

    if (!rule) {
        rule = { name: '<unknown>!' };
    }

    document.getElementById('host').insertAdjacentText('afterbegin', host);
    document.getElementById('container').insertAdjacentText('afterbegin', containername);
    document.getElementById('rulename').insertAdjacentText('afterbegin', rule.name);

    var reopen = document.getElementById('reopen');
    if (!rule)
        return;

    if (!containers.length) {
        reopen.insertAdjacentText('beforeend', `You haven\'t configured any containers for the "${rule.name}" rule.`);
    } else {
        for (let c of containers) {
            let link = create_container_box(c);
            link.addEventListener("click", evt => {
                browser.runtime.sendMessage({action: 'reopen_in_container', url: desturl, container: c.id}).catch(console.error);
            });

            reopen.insertAdjacentElement('beforeend', link);
        }
    }
    var disable_button = document.getElementById('disable');
    disable_button.addEventListener('click', evt => browser.runtime.sendMessage({ action: 'disable_and_reopen', rule_id, url: desturl }));
    disable_button.insertAdjacentText('beforeend', `Disable the "${rule.name}" rule`);

    var enable_button = document.getElementById('enable');
    enable_button.addEventListener('click', evt => browser.runtime.sendMessage({ action: 'enable_current_and_reopen', rule_id, url: desturl }));
    enable_button.insertAdjacentText('beforeend', `Enable "${containername}" in the "${rule.name}" rule`);

    document.getElementById('settings').addEventListener('click', evt => browser.runtime.sendMessage({ action: 'open_settings' }));
}

init().catch(console.error);

// ==Resource==
// @filename    options.js
// ==/Resource==

function build_rule_box(rule, all_containers, isnew) {
    var box = create_elem('fieldset', { class: 'rule-box' });
    box.appendChild(create_elem('legend', { text: `Rule #${rule.id}` }));

    var top_row = create_elem('div', { class: 'top-row' });
    box.appendChild(top_row);

    var name_entry = create_elem('input', { class: 'name-entry', size: '40', type: 'text', value: rule.name});
    top_row.appendChild(name_entry);

    var float_box = create_elem('div', { class: 'float-box' });
    top_row.appendChild(float_box);

    var enable_box = create_elem('label', { class: 'enable-box', for: `enable-button-${rule.id}` });
    float_box.appendChild(enable_box);

    var delete_button = create_elem('button', { class: 'delete-button', text: 'Delete' });
    float_box.appendChild(delete_button);

    var enable_checkbox = create_elem('input', { id: `enable-button-${rule.id}`, class: 'enable-checkbox', type: 'checkbox' });
    if (rule.enabled)
        enable_checkbox.setAttribute('checked', '1');
    enable_box.appendChild(enable_checkbox);

    enable_box.insertAdjacentText('beforeend', 'Enabled');

    var domain_box = create_elem('fieldset', { class: 'domain-box' });
    domain_box.appendChild(create_elem('legend', { text: 'Domains (one per line)' }));
    box.appendChild(domain_box);

    var domain_entry = create_elem('textarea', { class: 'domain-entry', rows: '30', cols: '50', text: rule.domains });
    domain_box.appendChild(domain_entry);

    var container_box = create_elem('fieldset', { class: 'container-box' });
    box.appendChild(container_box);

    container_box.appendChild(create_elem('legend', { text: 'Allowed in Containers' }));

    let container_boxes = [];
    function update_container_box_style(container, link) {
        var enabled = rule.containers.includes(container.id);
        link.classList.remove(enabled ? 'container-disabled' : 'container-enabled');
        link.classList.add(enabled ? 'container-enabled' : 'container-disabled');
    }

    for (let container of all_containers) {
        let link = create_container_box(container);
        container_box.appendChild(link);
        container_boxes.push({ container, link });
        link.addEventListener("click", evt => {
            if (rule.containers.includes(container.id)) {
                rule.containers = rule.containers.filter(c => c !== container.id);
            } else {
                rule.containers.push(container.id);
            }
            update_container_box_style(container, link);
            browser.runtime.sendMessage({action: 'update_rule_containers', rule_id: rule.id, containers: rule.containers}).catch(console.error);
        });
        update_container_box_style(container, link);
    }

    function update_name() {
        if (name_entry.value !== rule.name) {
            rule.name = name_entry.value;
            browser.runtime.sendMessage({action: 'update_rule_name', rule_id: rule.id, name: rule.name}).catch(console.error);
        }
    }

    function update_enabled() {
        if (enable_checkbox.checked !== rule.enabled) {
            rule.enabled = enable_checkbox.checked;
            browser.runtime.sendMessage({action: 'update_rule_enabled', rule_id: rule.id, enabled: rule.enabled}).catch(console.error);
        }
    }

    function update_domains() {
        if (domain_entry.value !== rule.domains) {
            rule.domains = domain_entry.value;
            browser.runtime.sendMessage({action: 'update_rule_domains', rule_id: rule.id, domains: rule.domains}).catch(console.error);
        }
    }

    function delete_rule() {
        browser.runtime.sendMessage({action: 'delete_rule', rule_id: rule.id }).catch(console.error);
        var parent = box.parentNode;
        parent.removeChild(box);
        if (!parent.querySelector('.rule-box')) {
            parent.innerHTML = "<p>No rules defined.</p>";
        }
    }

    name_entry.addEventListener('focus', evt => name_entry.setSelectionRange(0, name_entry.value.length));
    name_entry.addEventListener('blur', update_name);
    enable_checkbox.addEventListener('change', update_enabled);
    domain_entry.addEventListener('blur', update_domains);
    delete_button.addEventListener('click', delete_rule);
    if (isnew)
        setTimeout(() => name_entry.focus(), 1);

    return box;
}

async function new_rule(containers) {
    var rule = await browser.runtime.sendMessage({action: 'new_rule'}).catch(console.error);
    var maindiv = document.getElementById('main');
    maindiv.appendChild(build_rule_box(rule, containers, true));
}

async function init() {
    var { containers, domain_container_rules } = await browser.runtime.sendMessage({ action: 'get_containers' });

    var maindiv = document.getElementById('main');
    maindiv.innerHTML = '';
    if (!domain_container_rules.length) {
        maindiv.innerHTML = "<p>No rules defined.</p>";
    }
    for (let rule of domain_container_rules) {
        maindiv.appendChild(build_rule_box(rule, containers));
    }
    var new_button = document.getElementById('new-button');
    new_button.addEventListener('click', evt => {
        if (!maindiv.querySelector('.rule-box')) {
            maindiv.innerHTML = "";
        }

        new_rule(containers).catch(console.error);
    });
    document.getElementById('mainform').addEventListener("submit", evt => evt.preventDefault());
}

document.addEventListener("DOMContentLoaded", evt => init().catch(console.error));

// ==Resource==
// @filename    view_common.css
// ==/Resource==

button.container-link {
    cursor: pointer;
    background: #FFF;
    border: 1px solid #CCC;
    display: inline-block;

    border-radius: 5px;
    margin-top: 10px;
    margin-left: 10px;
    margin-right: 20px;
    padding: 10px;
}

button.container-link div.container-inner {
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.4);
    background: white;
    padding: 4px;
    border-radius: 4px;

}

button.container-link span {
    vertical-align: middle;
    margin-right: 6px;
    display: inline-block;
    width: 20px;
    height: 20px;

    background-position: center center;
    background-repeat: no-repeat;
    background-size: 20px 20px;
    filter: url('filters.svg#fill');
}

button.container-link span.noicon {
    width: 0px;
    margin: 0px;
}

fieldset {
    border-radius: 10px
}

legend {
    background-color: #EEEEFF;
    padding: 2px;
    border: 1px solid #E0E0F0;
    border-radius: 4px;
}

button {
    text-align: center;
    background-color: #EEEEFF;
    border: 1px solid #E0E0F0;
    border-radius: 4px;
    padding: 10px;
    margin-left: 10px;
    margin-right: 10px;
}

// ==Resource==
// @filename    block_message.css
// ==/Resource==

body {
    text-align: center;
    background-color: #DDD;
    font-family: sans-serif;
}

#messagebox {
    display: inline-block;
    margin-top: 6em;
    background-color: #FCC;
    padding: 1em;
    border: 1px solid #DAA;
    border-radius: 15px;
}

#reopen {
    display: inline-block;
    margin-left: auto;
    margin-right: auto;
}

img {
    float: left;
}

// ==Resource==
// @filename    options.css
// ==/Resource==

body {
    font-family: sans-serif;
}

p {
    text-align: center;
}

#mainform {
    text-align: center;
}

.rule-box {
    text-align: left;
    max-width: 800px;
    background-color: #FFF2E8;
    padding: 15px 30px;
    border: 1px solid #EDC;
    border-radius: 15px;
    margin: 10px auto 20px auto;
    display: block;
}

.top-row {
    height: 36px;
    line-height: 36px;
    margin-bottom: 15px;
}

.container-link.container-enabled {
    background-color: #DFD;
}

.container-link.container-disabled {
    background-color: #DAA;
}

.container-box {
    margin-top: 20px;
}

.name-entry {
    vertical-align: middle;
}

.enable-box {
    display: inline-block;

    text-align: center;

    background-color: #EEEEFF;
    padding-left: 10px;
    padding-right: 10px;
    border: 1px solid #E0E0F0;
    border-radius: 4px;

    line-height: 36px;
    width: 100px;
    height: 36px;
}

.enable-checkbox {
    vertical-align: middle;
}

.domain-box {
    clear: both;
}

.domain-entry {
    width: 100%
}

.delete-button {
    display: inline-block;
    margin-left: 20px;


    width: 100px;
    height: 36px;
}

.float-box {
    float: right;
}

// ==Resource==
// @filename    filters.svg
// ==/Resource==
<?xml version="1.0" encoding="UTF-8"?>
<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<svg xmlns="http://www.w3.org/2000/svg">
  <filter id="fill">
    <feComposite in="FillPaint" in2="SourceGraphic" operator="in"/>
  </filter>
</svg>

// ==Resource==
// @filename    block_message.html
// @web-accessible
// ==/Resource==
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Domain blocked outside of container</title>
    <script src="view_common.js"></script>
    <link rel="stylesheet" type="text/css" href="view_common.css">
    <link rel="stylesheet" type="text/css" href="block_message.css">
    <link rel="icon" type="image/png" href="block_outside_container.png">
  </head>
  <body>
    <div id="messagebox">
      <h1><img src="block_outside_container.png">"<span id="host"></span>" has been blocked from running in the "<span id="container"></span>" container by the "<span id="rulename"></span>" rule.</h1>
      <fieldset id="reopen"><legend>Reopen in:</legend></fieldset>
      <p><button id="disable"></button><button id="enable"></button><button id="settings">Settings</button></p>
    </div>
    <script src="block_message.js"></script>
  </body>
</html>

// ==Resource==
// @filename    options.html
// ==/Resource==
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="view_common.js"></script>
    <link rel="stylesheet" type="text/css" href="view_common.css">
    <link rel="stylesheet" type="text/css" href="options.css">
  </head>
  <body>
    <form action="#" id="mainform">
      <div id="main"></div>
      <button id="new-button">New Rule</button>
    </form>
    <script src="options.js"></script>
  </body>
</html>
